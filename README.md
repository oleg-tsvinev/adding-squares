# Interview question about adding images

First of all, it's very cool. Very visual, relatively easy to understand yet challenging.

## Code
If you want to jump into looking at the code, 
problem statement representation is in file
`ProblemStatementRepresentation`

If you want to go bottom-up, start with interface `Square`

This image shows data to operate on and the only operation, adding images.

![image](Image_Merge.png)

By torturing the interviewer with questions :), the following was found out:

1. We only deal with squares. 
2. It has nothing to do with geometry, sizes or coordinates
3. All what matters is the relationship between squares
4. We can only have four equal quarters inside a bigger square

Therefore the representation is much simpler than it seems. 
 
And this is what you find out reading the code.

I added methods and static final values to simplify representation. 
There's no way I could come up with this solution without prior knowledge, in 45 minutes.

The interviewer spoke Scala, I spoke Java.
This was an impediment, because Java does not have a match expression; 
it would be roughly an equivalent of a switch over object type,
perfectly acceptable in Scala but discouraged in Java.

## Notes 

After sleeping on square types, blank and shaded, I believe that 
the SquareProperty enum is unnecessary. Static instances provide enough 
information about the square type and even in case of adding new types
operations on them can be encapsulated in the interface Square,
because Java now allows static methods with implementations, in interfaces.   

Important to notice that adding BasicSquare and CompositeSquare is
an operation of adding the same BasicSquare to all BasicSquares that
CompositeSquare is composed of.

## Thus, we have three cases:
1. Adding BasicSquare to BasicSquare
2. Adding BasicSquare to CompositeSquare
3. Adding CompositeSquare to CompositeSquare

Another observation - once we have a blank square,
the sum is the other equivalent square no matter the type/color


