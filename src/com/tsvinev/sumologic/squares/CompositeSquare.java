package com.tsvinev.sumologic.squares;

public class CompositeSquare implements Square {

    final Square tl;
    final Square tr;
    final Square bl;
    final Square br;

    @Override
    public String toString() {
        return "tl: " + tl + ", tr:" + tr + ", bl:" + bl + ", br:" + br;
    }

    private CompositeSquare(Square TL, Square TR, Square BL, Square BR) {
        tl = TL;
        tr = TR;
        bl = BL;
        br = BR;
    }

    // static factory method to shorten the code
    public static final Square composite(Square TL, Square TR, Square BL, Square BR) {
        return new CompositeSquare(TL, TR, BL, BR);
    }
}
