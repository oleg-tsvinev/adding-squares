package com.tsvinev.sumologic.squares;

import static com.tsvinev.sumologic.squares.BasicSquare.blank;
import static com.tsvinev.sumologic.squares.BasicSquare.shaded;
import static com.tsvinev.sumologic.squares.CompositeSquare.composite;

public class ProblemStatementRepresentation {

    // Line one, left to right
    public static final Square s11 = blank;
    public static final Square s12 = blank;
    // result
    public static final Square s13 = blank;

    // Line two
    public static final Square s21 = shaded;
    public static final Square s22 = blank;
    // result
    public static final Square s23 = shaded;

    // Line three
    public static final Square s31 = blank;
    public static final Square s32 = composite(
            shaded,    // TL
            blank,     // TR
            blank,     // BL
            blank      // BR
    );
    // result
    public static final Square s33 = composite(
            shaded,
            blank,
            blank,
            blank
    );

    // Line four (Added a default constructor making BLANK square
    public static final Square s41 = shaded;
    public static final Square s42 = composite(
            blank,
            shaded,
            blank,
            blank
    );
    // result
    public static final Square s43 = shaded;

    public static final Square s51 = composite(blank, shaded, shaded, blank);
    public static final Square s52 = composite(
            composite(shaded, blank, blank, shaded),
            composite(blank, blank, blank, shaded),
            blank,
            composite(blank, blank, blank, shaded));
    // result
    public static final Square s53 = composite(
            composite(shaded, blank, blank, shaded),
            shaded,
            shaded,
            composite(blank, blank, blank, shaded));

}
