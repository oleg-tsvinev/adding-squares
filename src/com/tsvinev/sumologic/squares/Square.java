package com.tsvinev.sumologic.squares;

import static com.tsvinev.sumologic.squares.BasicSquare.blank;
import static com.tsvinev.sumologic.squares.BasicSquare.shaded;
import static com.tsvinev.sumologic.squares.CompositeSquare.composite;

public interface Square {
    // Important to notice that adding BasicSquare and CompositeSquare is
    // an operation of adding the same BasicSquare to all BasicSquares that
    // CompositeSquare is composed of.
    //
    // Thus, we have three cases:
    // 1. Adding BasicSquare to BasicSquare
    // 2. Adding BasicSquare to CompositeSquare
    // 3. Adding CompositeSquare to CompositeSquare
    //
    // Another observation - once we have a blank square,
    // the sum is the other equivalent square no matter the type/color
    //
    // And if fact, it's very simple - adding a BasicSquare B to CompositeSquare C
    // results into C if B is blank and B if B is shaded.
    //
    // Therefore the real problem is adding two CompositeSquare(s) and the solution to descend from
    // both until first BasicSquare is found, then
    // ie:
    //
    // sum(s1, s2) = both basic ? sum(s1, s2) : one basic? (we know) otherwise /* both composite */ {
    //   composite(sum(s1.tl, s2.tl), sum(s1.tr, s2.tr), sum(s1.bl, s2.bl), sum(s1.br, s2.br))
    static Square sum(Square s1, Square s2) {

        if (BasicSquare.class.isInstance(s1) && BasicSquare.class.isInstance(s2)) {
            return addBasic(s1, s2);
        }
        if (CompositeSquare.class.isInstance(s1) || CompositeSquare.class.isInstance(s2)) {
            if (CompositeSquare.class.isInstance(s1)) {
                return addBasicAndComposite(s1, s2);
            } else { // s2 is composite
                return addBasicAndComposite(s2, s1);
            }
        } else {
            // here we know both are composite
            CompositeSquare cs1 = CompositeSquare.class.cast(s1);
            CompositeSquare cs2 = CompositeSquare.class.cast(s2);
            return composite(sum(cs1.tl, cs2.tl), sum(cs1.tr, cs2.tr), sum(cs1.bl, cs2.bl), sum(cs1.br, cs2.br));
        }
    }

    // Note that we can literally compare instances here
    // because there's only two per JVM - yeah, singletons!

    static Square addBasicAndComposite(Square s1, Square s2) {
        if (blank == s2) {
            return s1;
        } else {
            return s2;
        }
    }

    static Square addBasic(Square s1, Square s2) {
        if (s1 == shaded || s2 == shaded) {
            return shaded;
        } else {
            return blank;
        }
    }
}
