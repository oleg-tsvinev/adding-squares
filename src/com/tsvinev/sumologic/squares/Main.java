package com.tsvinev.sumologic.squares;

import static com.tsvinev.sumologic.squares.BasicSquare.blank;
import static com.tsvinev.sumologic.squares.BasicSquare.shaded;
import static com.tsvinev.sumologic.squares.CompositeSquare.composite;
import static com.tsvinev.sumologic.squares.ProblemStatementRepresentation.s31;
import static com.tsvinev.sumologic.squares.ProblemStatementRepresentation.s32;
import static com.tsvinev.sumologic.squares.Square.sum;

public class Main {

    public static void main(String[] args) {
        Square s1 = blank;
        Square s2 = composite(shaded, blank, blank, blank);
        Square s3 = Square.sum(s1, s2);
        System.out.printf("Done: %s%n", s3);
        System.out.printf("Done: %s%n", sum(s31, s32));
        // TODO add real unit tests
    }
}
