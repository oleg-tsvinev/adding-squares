package com.tsvinev.sumologic.squares;

public class BasicSquare implements Square {

    private BasicSquare() {
    }

    @Override
    public String toString() {
        return this == blank ? "blank" : "shaded";
    }

    /**
     * static instances we can use,
     * because only the relationship between squares matter,
     * and the value is final ie unmodifiable,
     * we can build entire structures out of these two
     * and later add more property types if need be
     */
    public static final Square blank = new BasicSquare();
    public static final Square shaded = new BasicSquare();

}
